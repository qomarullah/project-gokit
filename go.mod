module project-gokit

go 1.15

require (
	github.com/go-kit/kit v0.10.0
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/lib/pq v1.9.0 // indirect
)
